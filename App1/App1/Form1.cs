﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;

namespace App1
{
    public partial class Check1 : Form
    {
        String nombre;
        List<String>listaDiferentes = new List<String>();
        List<int> listaCantidad = new List<int>();

        List<int> listaPalabras = new List<int>();

        public Check1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                this.Controls.OfType<TextBox>().ToList().ForEach(limpiar => limpiar.Text = "");
                this.Controls.OfType<CheckBox>().ToList().ForEach(apagar => apagar.Checked = false);
                nombre = openFileDialog1.FileName;
                label1.Text = openFileDialog1.FileName;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }


        private void button1_Click_1(object sender, EventArgs e)
        {

            if (nombre == null)
            {
                this.Controls.OfType<TextBox>().ToList().ForEach(limpiar => limpiar.Text = "");
                this.Controls.OfType<CheckBox>().ToList().ForEach(apagar => apagar.Checked = false);
                MessageBox.Show("Debe seleccionar un archivo", "Error");

            }
            else {
                if (checkBox1.Checked == true)
                {
                    Sel1();
                }

                if (checkBox2.Checked == true)
                {
                    Sel2();
                }
                if (checkBox3.Checked == true)
                {
                    Sel3();
                }
                if (checkBox4.Checked == true)
                {
                    Sel4();
                }

                if (checkBox5.Checked == true)
                {
                    Sel5();
                }

                if (checkBox6.Checked == true)
                {
                    Sel6();
                }
                if (checkBox7.Checked == true)
                {
                    Sel7();
                }
                if (checkBox8.Checked == true)
                {
                    Sel8();
                }
                if (checkBox9.Checked == true)
                {
                    palabras();
                }
            }

        }

        public void Sel1()
        {
            String palabraLarga = "";
            String palabra = "";
            String linea = "";
            int contador = 0;
            int numero = 0;
            int larga = 0;
            ArrayList tex = new ArrayList();

            if (nombre != "")
            {
                System.IO.StreamReader archivo = new System.IO.StreamReader(openFileDialog1.FileName);
                while (linea != null)
                {
                    for (int i = 0; i < linea.Length; i++)
                    {
                        if (char.IsLetter(linea[i]))
                        {
                            palabra += linea[i];
                            numero += 1;
                        }
                        else
                        {
                            if (palabra != "")
                            {
                                if (larga == 0)
                                {
                                    larga = numero;
                                }
                                else
                                {
                                    if (larga < numero)
                                    {
                                        larga = numero;
                                        palabraLarga = palabra;
                                    }
                                }
                                Console.WriteLine(palabra);
                                palabra = "";
                                numero = 0;
                                contador += 1;
                            }
                        }
                    }
                    tex.Add(linea);
                    linea = archivo.ReadLine();
                }
                textBox3.AppendText(palabraLarga);
                archivo.Close();
            }
        }

        public void Sel2()
        {
            int num = Convert.ToInt32(textBox2.Text);
            if (textBox2.Text == "")
            {
                MessageBox.Show("Debe ingresar un numero en el cuadro de abajo", "N palabras más comunes");
            }
            else {
                String linea = "";
                String palabra = "";
                List<String> ListaComunes = new List<String>();
                List<int> ListaCant = new List<int>();
                List<int> ListaPalabras = new List<int>();
                if (listaDiferentes == null) {
                    Sel5();
                    if (nombre != "")
                    {
                        Console.WriteLine("b");
                        System.IO.StreamReader archivo = new System.IO.StreamReader(openFileDialog1.FileName);
                        for (int var = 0; var < listaDiferentes.Count; var++) {
                            int contador = 0;
                            ArrayList tex = new ArrayList();
                            while (linea != null)
                            {
                                for (int i = 0; i < linea.Length; i++)
                                {
                                    if (char.IsLetter(linea[i]))
                                    {
                                        palabra += linea[i];
                                    }
                                    else
                                    {
                                        if (palabra != "")
                                        {
                                            if (listaDiferentes[var] == palabra)
                                            {
                                                contador += 1;
                                                palabra = "";
                                                Console.WriteLine("a");
                                            }
                                        }
                                    }
                                }
                                tex.Add(linea);
                                linea = archivo.ReadLine();
                            }
                            ListaComunes.Add(Convert.ToString(contador));
                            Console.WriteLine(ListaComunes);
                        }

                        archivo.Close();
                        int variable = 0;
                        while (variable < num) {
                            int mayor = 0;
                            for (int c = 0; c < ListaComunes.Count; c++) {
                                int numero = Convert.ToInt32(ListaComunes[c]);
                                if (mayor == 0) {
                                    mayor = numero;
                                }
                                else {
                                    if (mayor < numero)
                                    {
                                        mayor = numero;
                                    }
                                }
                            }
                            variable += 1;
                            int posicion = ListaComunes.IndexOf(Convert.ToString(mayor));
                            Console.WriteLine(posicion);
                            listView1.Columns.Add(listaDiferentes[posicion]);
                            ListaComunes.Remove(Convert.ToString(mayor));
                        }
                    }


                }

            }
        }


        public void Sel3()
        {

            String N_palabra = textBox1.Text;
            if (N_palabra == "")
            {
                MessageBox.Show("Debe ingresar la palabra en el textbox a la izquierda de la opción", "Repetición de una palabra");

                this.Controls.OfType<TextBox>().ToList().ForEach(limpiar => limpiar.Text = "");
                this.Controls.OfType<CheckBox>().ToList().ForEach(apagar => apagar.Checked = false);
            }
            else
            {
                String palabra = "";
                String linea = "";
                int contador = 0;
                ArrayList tex = new ArrayList();

                if (nombre != "")
                {
                    System.IO.StreamReader archivo = new System.IO.StreamReader(openFileDialog1.FileName);
                    while (linea != null)
                    {
                        for (int i = 0; i < linea.Length; i++)
                        {
                            if (char.IsLetter(linea[i]))
                            {
                                palabra += linea[i];
                            }
                            else
                            {
                                if (palabra != "")
                                {
                                    if (palabra == N_palabra)
                                    {
                                        contador += 1;
                                    }
                                    palabra = "";
                                }
                            }
                        }
                        tex.Add(linea);
                        linea = archivo.ReadLine();
                    }
                    textBox4.AppendText(Convert.ToString(contador));
                    archivo.Close();
                }

            }
        }

        public void Sel4()
        {
            String palabra = "";
            String linea = "";
            int contador = 0;
            ArrayList tex = new ArrayList();

            if (nombre != "")
            {
                System.IO.StreamReader archivo = new System.IO.StreamReader(openFileDialog1.FileName);
                while (linea != null)
                {
                    for (int i = 0; i < linea.Length; i++)
                    {
                        if (char.IsLetter(linea[i]))
                        {
                            palabra += linea[i];
                        }
                        else {
                            if (palabra != "")
                            {
                                Console.WriteLine(palabra);
                                palabra = "";
                                contador += 1;
                            }
                        }
                    }
                    tex.Add(linea);
                    linea = archivo.ReadLine();
                }
                textBox5.AppendText(Convert.ToString(contador));
                archivo.Close();
            }
        }
        public void Sel5()
        {
            var palabra = "";
            String linea = "";
            int contador = 0;
            int contador1 = 1;
            ArrayList tex = new ArrayList();

            if (nombre != "")
            {
                System.IO.StreamReader archivo = new System.IO.StreamReader(openFileDialog1.FileName);
                while (linea != null)
                {
                    for (int i = 0; i < linea.Length; i++)
                    {
                        if (char.IsLetter(linea[i]))
                        {
                            palabra += linea[i];
                            contador += 1;
                        }
                        else
                        {
                            if (palabra != "")
                            {
                                if (listaDiferentes == null)
                                {
                                    listaDiferentes[0] = palabra;                                
                                }
                                else
                                {
                                    if (listaDiferentes.Contains(palabra))
                                    {

                                        int posicion = Listadiferentes.IndexOf(palabra);
                                        if (ListaPalabras[posicion] == null)
                                        {
                                            ListaPalabras[posicion] = contador1;
                                        }
                                        else {
                                            int numero=listaPalabras[posicion];
                                            numero += 1;
                                            listaPalabras[posicion].Add(numero);
                                        }

                                    }
                                    else
                                    {
                                        listaPalabras.Add(contador1);
                                        listaDiferente.Add(palabra);
                                        listaCantidad.Add(contador);
                                    }
                                }
                                palabra = "";
                                contador = 0;

                            }
                        }
                    }
                    tex.Add(linea);
                    linea = archivo.ReadLine();
                }
                archivo.Close();
                if (checkBox5.Checked == true) {
                    textBox6.AppendText(Convert.ToString(listaDiferentes.Count));
                }

            }

        }


        public void Sel6()
        {
            String linea = "";
            int contador = 0;
            ArrayList tex = new ArrayList();

            if (nombre != "")
            {
                System.IO.StreamReader archivo = new System.IO.StreamReader(openFileDialog1.FileName);
                while (linea != null)
                {
                    tex.Add(linea);
                    contador += linea.Length;
                    linea = archivo.ReadLine();
                }
                textBox7.AppendText(Convert.ToString(contador));
                archivo.Close();
            }
        }

        public void Sel7()
        {
            String linea = "";
            int contador = 0;
            int espacios = 0;
            char letras;

            ArrayList tex = new ArrayList();

            if (nombre != "")
            {
                System.IO.StreamReader archivo = new System.IO.StreamReader(openFileDialog1.FileName);
                while (linea != null)
                {
                    contador += linea.Length;
                    for (int a = 0; a < linea.Length; a++)
                    {
                        letras = linea[a];
                        if (letras.Equals(' '))
                        {
                            espacios += 1;
                        }
                    }
                    tex.Add(linea);
                    linea = archivo.ReadLine();

                }
                contador = contador - espacios;
                textBox8.AppendText(Convert.ToString(contador));
                archivo.Close();
            }

        }
        public void Sel8()
        {
            String linea = "";
            int contador = 0;
            ArrayList tex = new ArrayList();

            if (nombre != "")
            {
                System.IO.StreamReader archivo = new System.IO.StreamReader(openFileDialog1.FileName);
                while (linea != null)
                {
                    for (int i = 0; i < linea.Length; i++) {
                        if (linea[i] == '.') {
                            contador += 1;
                        }
                    }
                    tex.Add(linea);
                    linea = archivo.ReadLine();
                }
                textBox9.AppendText(Convert.ToString(contador));
                archivo.Close();
            }

        }
  
 

        private void button2_Click(object sender, EventArgs e)
        {
            this.Controls.OfType<CheckBox>().ToList().ForEach(encender => encender.Checked = true);
        }

    }

}